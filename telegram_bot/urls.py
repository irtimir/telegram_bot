"""telegram_bot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from telegram_bot import settings
from webhook.views import TelegramBot
from utils_api.views import SetWebhook
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title='Telegram Bot API')

urlpatterns = [
    url('{}/'.format(settings.PREFIX_URL), include([
        url(r'admin/', admin.site.urls),
        url(r'docs/', include_docs_urls(title='Telegram Bot API')),
        url(r'webhook/$', TelegramBot.as_view()),
        url(r'set_webhook$', SetWebhook.as_view()),
        url(r'', include('auth.urls')),
        url(r'', include('dispatch_api.urls')),
        url(r'$', schema_view),
    ]))
]

urlpatterns = format_suffix_patterns(urlpatterns)
