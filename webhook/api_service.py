from pprint import pprint

import coreapi
# import requests
# from telegram_bot import settings

api_url = 'https://test.api.sendandsale.com/'


def auth_api_messenger(name, passdw):
    '''авторизация на API Service'''
    client = coreapi.Client()
    schema = client.get(api_url)
    result = client.action(schema, ['auth', 'create'], {'username': name, 'password': passdw})
    auth = coreapi.auth.TokenAuthentication(
        scheme='Token',
        token=result['data']['token']
    )
    client = coreapi.Client(auth=auth)
    return client


def get_sublist_uuid(list_uuid, name, passwd):
    '''Получение подписного листа по его uuid'''
    client = auth_api_messenger(name, passwd)
    schema = client.get(api_url)
    subs_list = client.action(schema, ['subscriptions', 'subslist', 'uuid', 'read'], params={'uuid': list_uuid})
    return subs_list


def create_client_subscriber(data_subscriber, name, passwd):
    '''Создание подписчика'''
    client = auth_api_messenger(name, passwd)
    schema = client.get(api_url)
    subscriber = client.action(schema, ['subscriptions', 'subscribers', 'create'], params=data_subscriber)
    return subscriber


def create_dialog(dialog_data, name, passwd):
    '''Создание диалога подписчика'''
    client = auth_api_messenger(name, passwd)
    schema = client.get(api_url)
    subscriber = client.action(schema, ['mess', 'dialogs', 'dialogs', 'create'], params=dialog_data)
    return subscriber


def send_subs_message(message_data, name, passwd):
    '''Отправитиь сообщение подписчика'''
    client = auth_api_messenger(name, passwd)
    schema = client.get(api_url)
    subscriber = client.action(schema, ['mess', 'dialogs', 'messages', 'create'], params=message_data)
    return subscriber


def get_all_subs(name, passwd):
    '''Получение подписчиков'''
    client = auth_api_messenger(name, passwd)
    schema = client.get(api_url)
    subs_list = client.action(schema, ['subscriptions', 'subscribers', 'list'])
    return subs_list['data']


def get_all_dialogs(name, passwd):
    '''Получение диалогов'''
    client = auth_api_messenger(name, passwd)
    schema = client.get(api_url)
    subs_list = client.action(schema, ['mess', 'dialogs', 'dialogs', 'list'])
    return subs_list['data']


def generic_send_message(message, subs_bot_id, name, passwd):
    '''Сохранение сообщения подписчика'''

    subs_id = None
    dialog_id = None
    message_data = None

    try:
        # получаем всех подписчиков и по ид бота подписчика получаем нужного
        all_subs = get_all_subs(name, passwd)
    except:
        return False
    else:
        for item in all_subs:
            if item['bot_id'] == str(subs_bot_id):
                subs_id = item['id']

    if subs_id:
        try:
            # получаем все диалоги и по ид подписчика получаем нужный
            all_dialogs = get_all_dialogs(name, passwd)
        except:
            return False
        else:
            for item in all_dialogs:
                if item['subscriber'] == subs_id:
                    dialog_id = item['id']
    else:
        return False

    if dialog_id:
        message_data = {
            'mess': '',
            'user_sender': False,  # TODO: True - отпревитель клиент, False - отправитель подписчик
            'text': message,
            'dialog': dialog_id
        }

    if message_data:
        try:
            send_subs_message(message_data, name, passwd)
        except:
            return False

    return True

# def r_auth_api_service():
#     result = requests.post(
#         url=api_url + '/auth/',
#         data={'username': settings.FACEBOOK['LOGIN'], 'password': settings.FACEBOOK['PASSWORD']}
#     )
#     return result.json()['data']['token']
#
#
# def get_subs_from_bot(bot_id):
#     '''Получение подписчика по ид бота'''
#     token = r_auth_api_service()
#     headers = ({'Authorization': 'Token {0}'.format(token)})
#     response = requests.get('https://test.api.sendandsale.com/subscriptions/subscribers', params={'bot_id': bot_id}, headers=headers)
#     return response.json()['data']
