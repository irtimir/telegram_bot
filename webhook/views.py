# import json
# from pprint import pprint
# from telegram_bot import settings
# import telebot
# from django.http import HttpResponseBadRequest, JsonResponse
# from django.utils.decorators import method_decorator
# from django.views.decorators.csrf import csrf_exempt
# from django.views.generic.base import View
# from .api_service import create_client_subscriber, get_sublist_uuid, create_dialog, generic_send_message
#
# bot = telebot.TeleBot(settings.TELEGRAM['TOKEN'])
# login = settings.TELEGRAM['LOGIN']
# password = settings.TELEGRAM['PASSWORD']
# type_messenger = 'telegram'
#
#
# @method_decorator(csrf_exempt, name='dispatch')
# class TelegramBot(View):
#     '''WebHook для Telegram'''
#
#     def post(self, request):
#         raw = request.body.decode('utf-8')
#         try:
#             payload = json.loads(raw)
#         except ValueError:
#             return HttpResponseBadRequest('Invalid request body!!!')
#         else:
#             # pprint(payload)
#             sender_id = payload['message']['from'].get('id')
#
#             if payload['message']['text'].startswith('/start'):
#
#                 # получаем uuid
#                 start_text = payload['message']['text'].split()
#                 # если бота открыли не по ссылке
#                 try:
#                     uuid = start_text[1]
#                 except IndexError:
#                     uuid = None
#
#                 if uuid:
#                     subs_list = get_sublist_uuid(uuid, login, password)
#                     list_id = subs_list['data'].get('id')
#                     data_subscriber = {
#                         'subs_list': list_id,
#                         'bot_id': sender_id,
#                         'name_messenger': 'telegram',
#                         'username': payload['message']['from'].get('username'),
#                         'first_name': payload['message']['from'].get('first_name') if payload['message']['from'].get(
#                             'first_name') else '',
#                         'last_name': payload['message']['from'].get('last_name') if payload['message']['from'].get(
#                             'last_name') else '',
#                         'subscribed': True,
#                     }
#                     try:
#                         subscriber = create_client_subscriber(data_subscriber, login, password)
#                         subscriber_id = subscriber['data']['id']  # id подписчика
#                         client_id = subs_list['data']['user']  # id клиента
#                         dialog_data = {
#                             'subscriber': subscriber_id,
#                             'user': client_id
#                         }
#                         create_dialog(dialog_data, login, password)
#                         bot.send_message(sender_id, 'Поздравляю, Вы подписались на наш канал.')
#                     except:
#                         bot.send_message(sender_id, 'Похоже Вы уже подписаны на наш канал.')
#             else:
#
#                 message = payload['message']['text']
#                 subs_bot_id = payload['message']['from']['id']
#
#                 if generic_send_message(message, subs_bot_id, login, password):
#                     bot.send_message(sender_id, 'Ваше сообщение отправлено, ожидайте ответа...')
#
#         return JsonResponse({}, status=200)


import json
from pprint import pprint
import telebot
from telegram_bot import settings
from django.http import HttpResponseBadRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from .api_service import create_client_subscriber, get_sublist_uuid, create_dialog, generic_send_message

bot = telebot.TeleBot(settings.TELEGRAM['TOKEN'])
login = settings.TELEGRAM['LOGIN']
password = settings.TELEGRAM['PASSWORD']
type_messenger = 'telegram'


@method_decorator(csrf_exempt, name='dispatch')
class TelegramBot(View):
    '''WebHook для Telegram'''

    def post(self, request):
        raw = request.body.decode('utf-8')
        try:
            payload = json.loads(raw)
        except ValueError:
            return HttpResponseBadRequest('Invalid request body!!!')
        else:
            pprint(payload)
            update = telebot.types.Update.de_json(payload)
            bot.process_new_updates([update])
        return JsonResponse({}, status=200)


@bot.message_handler(commands=['start'])
def add_subscriber(message):
    # получаем uuid
    start_text = message.text.split()
    # если бота открыли не по ссылке
    try:
        uuid = start_text[1]
    except IndexError:
        uuid = None

    if uuid:
        subs_list = get_sublist_uuid(uuid, login, password)
        list_id = subs_list['data'].get('id')
        data_subscriber = {
            'subs_list': list_id,
            'bot_id': message.from_user.id,
            'client_bot': bot.get_me().id,
            'name_messenger': 'telegram',
            'username': message.from_user.username,
            'first_name': message.from_user.first_name if message.from_user.first_name else '',
            'last_name': message.from_user.last_name if message.from_user.last_name else '',
            'subscribed': True,
        }
        try:
            subscriber = create_client_subscriber(data_subscriber, login, password)
            subscriber_id = subscriber['data']['id']  # id подписчика
            client_id = subs_list['data']['user']  # id клиента
            dialog_data = {
                'subscriber': subscriber_id,
                'user': client_id
            }
            create_dialog(dialog_data, login, password)
            bot.send_message(
                message.chat.id,
                'Поздравляю, Вы подписались на наш канал.'
            )
        except:
            bot.send_message(
                message.chat.id,
                'Похоже Вы уже подписаны на наш канал.'
            )


@bot.message_handler(func=lambda message: True, content_types=['text'])
def text_from_subscriber(message):
    bot.send_message(message.chat.id, 'Ваше сообщение отправлено, ожидайте ответа...')

    # if generic_send_message(message.text, message.from_user.id, login, password):
    #     bot.send_message(message.chat.id, 'Ваше сообщение отправлено, ожидайте ответа...')


@bot.message_handler(content_types=['photo'])
def handle_docs_audio(message):
    caption = message.caption  # подпись для фото
    file_info = bot.get_file(message.photo[-1].file_id)
    file_url = 'https://api.telegram.org/file/bot{0}/{1}'.format(settings.TELEGRAM['TOKEN'], file_info.file_path)
    print(file_url)
    print('подписчик {} отправил фото'.format(message.chat.id))


@bot.message_handler(content_types=['document'])
def handle_docs_audio(message):
    file_info = bot.get_file(message.document.file_id)
    # file = requests.get(
    #     'https://api.telegram.org/file/bot{0}/{1}'.format(settings.TELEGRAM['TOKEN'], file_info.file_path))
    file_url = 'https://api.telegram.org/file/bot{0}/{1}'.format(settings.TELEGRAM['TOKEN'], file_info.file_path)
    print(file_url)
    print('подписчик {} отправил файл'.format(message.chat.id))


@bot.message_handler(content_types=['audio'])
def handle_docs_audio(message):
    file_info = bot.get_file(message.audio.file_id)
    file_url = 'https://api.telegram.org/file/bot{0}/{1}'.format(settings.TELEGRAM['TOKEN'], file_info.file_path)
    print(file_url)
    print('подписчик {} отправил аудиофал'.format(message.chat.id))
