from rest_framework import serializers


class BaseClassSerializer(serializers.Serializer):
    '''Базовый сериализатор для рассылки'''
    subscriber = serializers.CharField(help_text='id пользователей Telegram для рассылки')


class TextMessageSerializer(BaseClassSerializer):
    '''Сериализатор для рассылки текстовых сообщений'''
    text_message = serializers.CharField(help_text='Сообщение для рассылки')


class UrlMessageSerializer(BaseClassSerializer):
    '''Сериализатор для рассылки изображений, видео'''
    url_message = serializers.URLField(help_text='URL изображения, видео')
