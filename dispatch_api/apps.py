from django.apps import AppConfig


class DispatchApiConfig(AppConfig):
    name = 'dispatch_api'
