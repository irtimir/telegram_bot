from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from telegram_bot import settings
from .serializers import TextMessageSerializer, UrlMessageSerializer

import telebot

t_bot = telebot.TeleBot(settings.TELEGRAM['TOKEN'])


class SendTextMessage(generics.GenericAPIView):
    '''api для отправки текстовых сообщений'''
    serializer_class = TextMessageSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            payload = serializer.data

            try:
                t_bot.send_message(payload['subscriber'], payload['text_message'])
                print('отправлено -', payload['subscriber'])
            except Exception as ex:
                print('Исключение:', ex)
                print('не отправлено -', payload['subscriber'])
                # TODO: отмечать если не отправлено
                # TODO: отмечать если привышен лимит отправки
                return Response({'status': 'error', 'message': 'message does not send', 'data': str(ex)},
                                status=status.HTTP_400_BAD_REQUEST)

            return Response({'status': 'success', 'message': None, 'data': serializer.data}, status=status.HTTP_200_OK)
        return Response({'status': 'error', 'message': serializer.error_messages, 'data': serializer.errors},
                        status=status.HTTP_400_BAD_REQUEST)


class SendImageMessage(generics.GenericAPIView):
    '''api для отправки изображений'''
    serializer_class = UrlMessageSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            payload = serializer.data

            try:
                t_bot.send_photo(payload['subscriber'], payload['url_message'])
                print('отправлено -', payload['subscriber'])
            except Exception as ex:
                print('Исключение:', ex)
                print('не отправлено -', payload['subscriber'])
                # TODO: отмечать если не отправлено
                # TODO: отмечать если привышен лимит отправки
                return Response({'status': 'error', 'message': 'message does not send', 'data': str(ex)},
                                status=status.HTTP_400_BAD_REQUEST)

            return Response({'status': 'success', 'message': None, 'data': serializer.data}, status=status.HTTP_200_OK)
        return Response({'status': 'error', 'message': serializer.error_messages, 'data': serializer.errors},
                        status=status.HTTP_400_BAD_REQUEST)


class SendVideoMessage(generics.GenericAPIView):
    '''api для отправки видео'''
    serializer_class = UrlMessageSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            payload = serializer.data

            try:
                t_bot.send_video(payload['subscriber'], payload['url_message'])
                print('отправлено -', payload['subscriber'])
            except Exception as ex:
                print('Исключение:', ex)
                print('не отправлено -', payload['subscriber'])
                # TODO: отмечать если не отправлено
                # TODO: отмечать если привышен лимит отправки
                return Response({'status': 'error', 'message': 'message does not send', 'data': str(ex)},
                                status=status.HTTP_400_BAD_REQUEST)

            return Response({'status': 'success', 'message': None, 'data': serializer.data}, status=status.HTTP_200_OK)
        return Response({'status': 'error', 'message': serializer.error_messages, 'data': serializer.errors},
                        status=status.HTTP_400_BAD_REQUEST)
