from django.conf.urls import url
from .views import SendTextMessage, SendImageMessage, SendVideoMessage

app_name = 'dispatch_api'

urlpatterns = [
    url(r'send_text_message/$', SendTextMessage.as_view(), name='send_text_message'),
    url(r'send_image_message/$', SendImageMessage.as_view(), name='send_image_message'),
    url(r'send_video_message/$', SendVideoMessage.as_view(), name='send_video_message'),
]
