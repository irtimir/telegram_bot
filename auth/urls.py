from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'auth/$', views.AuthTokenList.as_view()),
    url(r'auth/(?P<pk>[0-9]+)/$', views.AuthTokenDetail.as_view()),
]