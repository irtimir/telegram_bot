from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'first_name', 'last_name', 'is_active', 'last_login', 'date_joined')
        read_only_fields = ('last_login', 'date_joined',)