from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from telegram_bot import settings
from .serializers import WebhookSerializer


class SetWebhook(generics.GenericAPIView):
    '''Установка вебхука'''
    serializer_class = WebhookSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            payload = serializer.data

            import telebot
            t_bot = telebot.TeleBot(settings.TELEGRAM['TOKEN'])
            t_bot.remove_webhook()

            if t_bot.set_webhook(url=payload['webhook']):
                print('true')
                pass  # TODO: подтвердить, что вебхук прописался
            else:
                print('false')
                pass  # TODO: сообщить, что вебхук не прописался
                return Response({'status': 'error', 'message': 'webhook does not set'},
                                status=status.HTTP_400_BAD_REQUEST)

            return Response({'status': 'success', 'message': None, 'data': serializer.data}, status=status.HTTP_200_OK)
        return Response({'status': 'error', 'message': serializer.error_messages, 'data': serializer.errors},
                        status=status.HTTP_400_BAD_REQUEST)
