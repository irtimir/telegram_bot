from django.apps import AppConfig


class DispatchApiConfig(AppConfig):
    name = 'utils_api'
