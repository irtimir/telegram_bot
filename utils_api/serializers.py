from rest_framework import serializers


class WebhookSerializer(serializers.Serializer):
    webhook = serializers.URLField(help_text='URL для вебхука')
